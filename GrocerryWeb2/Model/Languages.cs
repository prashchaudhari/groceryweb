﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GrocerryWeb2.Model
{
    public class Languages
    {
        [Key]
        public Guid LanguageID { get; set; }
        public string Title { get; set; }
        public string Locale { get; set; }
        public bool Status { get; set; }
        public Nullable<Guid> Created_User { get; set; }
        public Nullable<DateTime> Created_Date { get; set; }
        public Nullable<Guid> Modified_User { get; set; }
        public Nullable<DateTime> Modified_Date { get; set; }
        public Nullable<bool> Deleted { get; set; }
        public Nullable<Guid> Deleted_By { get; set; }
        public Nullable<DateTime> Deleted_Date { get; set; }
    }
}
