﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GrocerryWeb2.Model
{
    public class CityTranslation
    {
        [Key]
        public Guid ID { get; set; }
        public Guid CityID { get; set; }
        public string Description { get; set; }
        public Guid LanguageID { get; set; }
        public Nullable<Guid> Created_User { get; set; }
        public Nullable<DateTime> Created_Date { get; set; }
        public Nullable<Guid> Modified_User { get; set; }
        public Nullable<DateTime> Modified_Date { get; set; }
        public Nullable<bool> Deleted { get; set; }
        public Nullable<Guid> Deleted_By { get; set; }
        public Nullable<DateTime> Deleted_Date { get; set; }

        public City city { get; set; }
        public Languages language { get; set; }
    }
}
