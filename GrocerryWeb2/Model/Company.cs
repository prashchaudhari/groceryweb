﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GrocerryWeb2.Model
{
    public class Company
    {
        [Key]
        public Guid ID { get; set; }        
        public string Code { get; set; }
        public string District { get; set; }
        public Guid CityID { get; set; }
        public Guid CountryID { get; set; }
        public string Phone { get; set; }
        public string State { get; set; }
        public string Pincode { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Registered { get; set; }
        public bool InActive { get; set; }
        public Nullable<Guid> Created_User { get; set; }
        public Nullable<DateTime> Created_Date { get; set; }
        public Nullable<Guid> Modified_User { get; set; }
        public Nullable<DateTime> Modified_Date { get; set; }
        public Nullable<bool> Deleted { get; set; }
        public Nullable<Guid> Deleted_By { get; set; }
        public Nullable<DateTime> Deleted_Date { get; set; }
        
    }
}
