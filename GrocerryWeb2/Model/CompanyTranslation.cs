﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GrocerryWeb2.Model
{
    public class CompanyTranslation
    {
        [Key]
        public Guid ID { get; set; }
	    public string Name { get; set; }
        public string MailingName { get; set; }
        public string Address { get; set; }
        public string ContactPerson { get; set; }
        public Guid CompanyID { get; set; }
        public Guid LanguageID { get; set; }
        public Nullable<Guid> Created_User { get; set; }
        public Nullable<DateTime> Created_Date { get; set; }
        public Nullable<Guid> Modified_User { get; set; }
        public Nullable<DateTime> Modified_Date { get; set; }
        public Nullable<bool> Deleted { get; set; }
        public Nullable<Guid> Deleted_By { get; set; }
        public Nullable<DateTime> Deleted_Date { get; set; }

        public Company companies { get; set; }
        public Languages languages { get; set; }
    }
}
