﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrocerryWeb2.Model
{
    public class ProductDetails
    {
        [Key]
       public Guid ProductdetailsID {get;set;}
       public string EAN {get;set;}
       public Guid ProductID {get;set;}
       public Guid BaseUnit {get;set;}
       public Guid UOMID {get;set;}
       public decimal Conv {get;set;}
       public decimal Price {get;set;}
       public decimal PriceA {get;set;}
       public decimal PriceB {get;set;}
       public decimal PriceC {get;set;}
       public decimal LeastPrice {get;set;}
       public decimal DiscountPrice {get;set;}
       public decimal DiscountPercentage {get;set;}
       public bool OnSale {get;set;}
       public decimal OfferSalePrice {get;set;}
       public DateTime SaleStart {get;set;}
       public DateTime SaleEnd {get;set;}
       public Nullable<Guid> PromotionID {get;set;}
       public decimal Comision {get;set;}
       public decimal ComisionPercentage {get;set;}

       public Nullable<Guid> Created_User { get; set; }
       public Nullable<DateTime> Created_Date { get; set; }
       public Nullable<Guid> Modified_User { get; set; }
       public Nullable<DateTime> Modified_Date { get; set; }
       public Nullable<bool> Deleted { get; set; }
       public Nullable<Guid> Deleted_By { get; set; }
       public Nullable<DateTime> Deleted_Date { get; set; }

       public Product Product { get; set; } 
    }
}
