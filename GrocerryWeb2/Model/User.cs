﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace GrocerryWeb2.Model
{
    [Table("Users")]
    public class User
    {
        [Key]
        public Guid ID { get; set; }
        public string POSCode { get; set; }
        public string Password { get; set; }
        public Guid RoleID { get; set; }
        public string BioMatric { get; set; }
        public bool InActive { get; set; }
        public Guid City { get; set; }
        public Guid Country { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string Pincode { get; set; }
        public string District { get; set; }
        public string PhoneNo { get; set; }
        public string MobileNo { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public Nullable<Guid> Created_User { get; set; }
        public Nullable<DateTime> Created_Date { get; set; }
        public Nullable<Guid> Modified_User { get; set; }
        public Nullable<DateTime> Modified_Date { get; set; }
        public Nullable<bool> Deleted { get; set; }
        public Nullable<Guid> Deleted_By { get; set; }
        public Nullable<DateTime> Deleted_Date { get; set; }

       // Role role { get; set; }
    }
}
