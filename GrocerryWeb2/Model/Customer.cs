﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrocerryWeb2.Model
{
    public class Customer
    {

        [Key]
        public Guid ID { get; set; }
        public string MailingName { get; set; }
        public string Address { get; set; }
        public Guid CountyID { get; set; }
        public string Colorcode { get; set; }
        public string State { get; set; }
        public string PinCode { get; set; }
        public string Code { get; set; }
        public string PhoneNo { get; set; }
        public string Mob_no { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Tan { get; set; }
        public string Gst { get; set; }
        public string Acc_no { get; set; }
        public string BankName { get; set; }
        public string BankBranch { get; set; }
        public string IFCCode { get; set; }

        public Nullable<Guid> Created_User { get; set; }
        public Nullable<DateTime> Created_Date { get; set; }
        public Nullable<Guid> Modified_User { get; set; }
        public Nullable<DateTime> Modified_Date { get; set; }
        public Nullable<bool> Deleted { get; set; }
        public Nullable<Guid> Deleted_By { get; set; }
        public Nullable<DateTime> Deleted_Date { get; set; }
    }
}
