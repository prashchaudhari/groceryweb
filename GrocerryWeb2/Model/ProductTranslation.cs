﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrocerryWeb2.Model
{
    public class ProductTranslation
    {
        [Key]
        public Guid ID { get; set; }
        public Guid ProductID { get; set; }
        public string Description { get; set; }
        public Nullable<Guid> Created_User { get; set; }
        public Nullable<DateTime> Created_Date { get; set; }
        public Nullable<Guid> Modified_User { get; set; }
        public Nullable<DateTime> Modified_Date { get; set; }
        public Nullable<bool> Deleted { get; set; }
        public Nullable<Guid> Deleted_By { get; set; }
        public Nullable<DateTime> Deleted_Date { get; set; }

        public Product Product { get; set; }
        public Guid LanguageID { get; set; }
        public Languages Language { get; set; }
    }
}
