﻿using System;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrocerryWeb2.Model
{
    public class Product
    {
        [Key]
        public Guid ProductID {get;set;}
        public Guid ProductTypeID {get;set;}
        public string CODE {get;set;}
        public string OLD_CODE {get;set;}
        public string EANBaseUnit {get;set;}
        public string PerishableDays {get;set;}
        public bool ProductSerial {get;set;}
        public bool ProductBatch {get;set;}
        public decimal purchasecost { get; set; }
        public string HSNCode { get; set; }
        public bool Perishable {get;set;}
        public bool Inactive {get;set;}
        public Nullable<Guid> Created_User { get; set; }
        public Nullable<DateTime> Created_Date { get; set; }
        public Nullable<Guid> Modified_User { get; set; }
        public Nullable<DateTime> Modified_Date { get; set; }
        public Nullable<bool> Deleted { get; set; }
        public Nullable<Guid> Deleted_By { get; set; }
        public Nullable<DateTime> Deleted_Date { get; set; }
        public Guid BrandID { get; set; }
        public Guid CategoryID { get; set; }
        public Guid SubCategoryID { get; set; }
        public Guid GroupID { get; set; }
        public Guid DivisionID { get; set; }
        public Guid TaxID { get; set; }
        public Guid PurchaseBaseUnitID { get; set; }
        public Guid BaseUnitID { get; set; }
    }
}
