import { Component, OnInit } from '@angular/core';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { BrandService } from '../../Services/brandservice.service';

@Component({
    selector: 'brandAdd',
    templateUrl: './brandAdd.component.html',
    providers: [
        BrandService
    ],
})
export class BrandAddComponent implements OnInit {

    BrandForm: FormGroup;
    title: string = 'Create';
    ID: any = 0;
    errorMessage: any;

    constructor(private _fb: FormBuilder, private _avRoute: ActivatedRoute, private _router: Router, private _brandService: BrandService) {

        this.ID = this._avRoute.snapshot.paramMap.get("id");
        this._avRoute.queryParams.subscribe(params => {
            let id = params['id'];
            this.ID = id;// Print the parameter to the console. 
        });
       
        this.BrandForm = this._fb.group({
            id: '0',
            code: ['', [Validators.required]],
            colorcode: ['', [Validators.required]],
            //Description: ['', [Validators.required]]
        });
    } 

    save() {
        if (!this.BrandForm.valid) {
            return;
        }  
        

        if (this.title == "Create") {
            this._brandService.saveBrand(this.BrandForm.value)
                .subscribe((data) => {
                    this._router.navigate(['/brandManage']);
                }, error => this.errorMessage = error)
            
        }
        else if (this.title == "Edit") {
            this._brandService.updateBrand(this.ID,this.BrandForm.value)
                .subscribe((data) => {
                    this._router.navigate(['/brandManage']);
                }, error => this.errorMessage = error)
        }  
        
    }

    ngOnInit() {
        
        
        if (this.ID.length > 1) {
            this.title = 'Edit';
            
            this._brandService.getBrandById(this.ID)
                .subscribe(resp => { this.BrandForm.setValue(resp); console.log(resp) }
                , error => this.errorMessage = error);  
        }
    }


    cancel() {
        this._router.navigate(['/brandManage']);
    }

    //get ID() { return this.BrandForm.get('ID');}
    get Code() { return this.BrandForm.get('Code'); }
    get Colorcode() { return this.BrandForm.get('Colorcode'); }
    //get Description() { return this.BrandForm.get('Description'); }


}