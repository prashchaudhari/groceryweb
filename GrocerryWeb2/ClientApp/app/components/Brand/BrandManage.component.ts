import { Component } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { BrandService } from '../../Services/brandservice.service';

@Component({
    selector: 'brandManage',
    templateUrl: './brandManage.component.html',
    providers: [
        BrandService
    ],
})
export class BrandManageComponent {
    public BrandList: BrandData[];

    constructor(public http: Http, private _router: Router, private _brandService: BrandService) {
        
    } 
    ngOnInit() {
        this._brandService.getBrands().subscribe((tempdate) => { this.BrandList = tempdate; })
            , err => {
                console.log(err);
            }
    } 
    getBrand() {
        this._brandService.getBrands().subscribe((tempdate) => {
        this.BrandList = tempdate;
            //console.log("abcc", this.BrandList);
        })
            , err => {
                console.log(err);
            }
        
    }
    edit(id) {
        this._router.navigate(['/brandAdd'], { queryParams: { id: id } });
    }

}


export interface BrandData {
    code:string,
    colorcode:string,
    created_Date : null,
    created_User :  null,
    deleted  : true,
    deleted_By : null,
    deleted_Date :  null,
    id : string,
    modified_Date: null,
    modified_User :  null,
}