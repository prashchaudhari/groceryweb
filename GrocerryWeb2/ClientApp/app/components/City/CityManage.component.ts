import { Component } from '@angular/core';
import { CityService } from '../../Services/cityservice.service';
import { Router } from '@angular/router';
import { Http } from '@angular/http';

@Component({
    selector: 'cityManage',
    templateUrl: './cityManage.component.html'
})
export class CityManageComponent {
    CityList: any;
 
  
    public BrandList: CityData[];

    constructor(public http: Http, private _router: Router, private _cityService: CityService) {

    }
    ngOnInit() {
        this._cityService.getCity().subscribe((tempdate) => { this.CityList = tempdate; })
            , err => {
                console.log(err);
            }
    }
    getBrand() {
        this._cityService.getCity().subscribe((tempdate) => {
            this.CityList = tempdate;
            //console.log("abcc", this.BrandList);
        })
            , err => {
                console.log(err);
            }

    }
   
}
export interface CityData {
    citycode: string,
    Description: string,
    created_Date: null,
    created_User: null,
    deleted: true,
    deleted_By: null,
    deleted_Date: null,
    id: string,
    modified_Date: null,
    modified_User: null,

}