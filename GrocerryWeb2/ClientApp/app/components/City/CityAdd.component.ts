import { Component, OnInit } from '@angular/core';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { CityService } from '../../Services/cityservice.service';


@Component({
    selector: 'CityAdd',
    templateUrl: './CityAdd.component.html',
    providers: [
        CityService
    ],
})
export class CityAddComponent implements OnInit  {
    CityForm: FormGroup;
    title: string = 'Create';
    ID: any = 0;
    errorMessage: any;

    constructor(private _fb: FormBuilder, private _avRoute: ActivatedRoute, private _router: Router, private _cityservice: CityService) {

        this.ID = this._avRoute.snapshot.paramMap.get("id");
        this._avRoute.queryParams.subscribe(params => {
            let id = params['id'];
            this.ID = id;// Print the parameter to the console. 
        });

        this.CityForm = this._fb.group({
            id: '0',
            citycode: ['', [Validators.required]],
            Description: ['', [Validators.required]],
            //Description: ['', [Validators.required]]
        });
    } 
   
        save() {
            if (!this.CityForm.valid) {
                return;
            }


            if (this.title == "Create") {
                this.
                    _cityservice.saveCity(this.CityForm.value)
                    .subscribe((data) => {
                        this._router.navigate(['/cityManage']);
                    }, error => this.errorMessage = error)

            }
            else if (this.title == "Edit") {
                this._cityservice.updateCity(this.ID, this.CityForm.value)
                    .subscribe((data) => {
                        this._router.navigate(['/cityManage']);
                    }, error => this.errorMessage = error)
            }
           }
      


    ngOnInit() {


        if (this.ID.length > 1) {
            this.title = 'Edit';

            this._cityservice.getCityById(this.ID)
                .subscribe(resp => { this.CityForm.setValue(resp); console.log(resp) }
                , error => this.errorMessage = error);
        }
    }


    cancel() {
        this._router.navigate(['/cityManage']);
    }
    get citycode() { return this.CityForm.get('citycode'); }
    get Description() { return this.CityForm.get('Description'); }
   
   


}