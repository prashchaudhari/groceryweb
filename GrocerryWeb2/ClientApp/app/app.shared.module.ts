import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { FetchDataComponent } from './components/fetchdata/fetchdata.component';
import { CounterComponent } from './components/counter/counter.component';
import { BrandAddComponent } from './components/Brand/BrandAdd.component';
import { BrandManageComponent } from './components/Brand/BrandManage.component';
import { CityAddComponent } from './components/City/CityAdd.component';
import { CityManageComponent } from './components/City/CityManage.component';
import { CotagoryAddComponent } from './components/Catogary/CatogaryAdd.component';
import { CurrencyAddComponent } from './components/Currency/CurrencyAdd.component';
import { CustomerAddComponent } from './components/Customer/CustomerAdd.component';
import { DivisionAddComponent } from './components/Division/DivisionAdd.component';
import { Component } from '@angular/core/src/metadata/directives';

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        CounterComponent,
        FetchDataComponent,
        HomeComponent,
        BrandAddComponent,
        BrandManageComponent,
        CityAddComponent,
        CityManageComponent,
        CotagoryAddComponent,
        CurrencyAddComponent,
        CustomerAddComponent,
        DivisionAddComponent
    ],
    
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'counter', component: CounterComponent },
            { path: 'fetch-data', component: FetchDataComponent },
            { path: 'brandAdd', component: BrandAddComponent },
            { path: 'brandAdd/:id', component: BrandAddComponent },
            { path: 'brandManage', component: BrandManageComponent },
            { path: 'cityAdd', component: CityAddComponent },
            { path: 'cityManage', component: CityManageComponent },
            { path: 'catogaryAdd', component: CotagoryAddComponent },
            { path: 'currencyAdd', component: CurrencyAddComponent },
            { path: 'customerAdd', component: CustomerAddComponent },
            { path: 'divisionAdd', component: DivisionAddComponent },
            { path: 'divisionAdd', component: DivisionAddComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ]
})
export class AppModuleShared {
}
