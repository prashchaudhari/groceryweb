﻿import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';  

@Injectable()

export class BrandService {
    myAppUrl: string = "";
    constructor(private _http: Http, @Inject('BASE_URL') baseUrl: string) {
        this.myAppUrl = baseUrl;
    }
    getBrands() {
        return this._http.get(this.myAppUrl + 'api/Brands')
            .map((response: Response) => response.json())
            .catch(this.errorHandler);
    }
    getBrandById(ID: AAGUID) {
        return this._http.get(this.myAppUrl + "api/Brands/" + ID)
            .map((response: Response) => response.json())
            .catch(this.errorHandler)
    }
    saveBrand(brand) {
        return this._http.post(this.myAppUrl + 'api/Brands/', brand)
            .map((response: Response) => response.json())
            .catch(this.errorHandler)
    }
    updateBrand(id,brand) {
        return this._http.put(this.myAppUrl + 'api/Brands/'+id, brand)
            .map((response: Response) => response.json())
            .catch(this.errorHandler);
    }
    deleteBrand(ID) {
        return this._http.delete(this.myAppUrl + "api/Brands/Delete/" + ID)
            .map((response: Response) => response.json())
            .catch(this.errorHandler);
    }
    errorHandler(error: Response) {
        console.log(error);
        return Observable.throw(error);
    } 
}