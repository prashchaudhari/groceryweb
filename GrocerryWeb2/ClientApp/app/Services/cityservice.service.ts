﻿import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';  

@Injectable()

export class CityService {
    myAppUrl: string = "";
    constructor(private _http: Http, @Inject('BASE_URL') baseUrl: string) {
        this.myAppUrl = baseUrl;
    }
    getCity() {
        return this._http.get(this.myAppUrl + 'api/City')
            .map((response: Response) => response.json())
            .catch(this.errorHandler);
    }
    getCityById(ID: AAGUID) {
        return this._http.get(this.myAppUrl + "api/City/" + ID)
            .map((response: Response) => response.json())
            .catch(this.errorHandler)
    }
    saveCity(city) {
        return this._http.post(this.myAppUrl + 'api/City/', city)
            .map((response: Response) => response.json())
            .catch(this.errorHandler)
    }
    updateCity(id,city) {
        return this._http.put(this.myAppUrl + 'api/City/' + id, city)
            .map((response: Response) => response.json())
            .catch(this.errorHandler);
    }
    deleteCity(ID) {
        return this._http.delete(this.myAppUrl + "api/City/Delete/" + ID)
            .map((response: Response) => response.json())
            .catch(this.errorHandler);
    }
    errorHandler(error: Response) {
        console.log(error);
        return Observable.throw(error);
    } 
}