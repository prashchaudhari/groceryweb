﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GrocerryWeb2.Migrations
{
    public partial class MyMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CityID = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    CountryID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    District = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    InActive = table.Column<bool>(nullable: false),
                    Mobile = table.Column<string>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Pincode = table.Column<string>(nullable: true),
                    Registered = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Website = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Currencies",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    ISO = table.Column<string>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    Symbol = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currencies", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Acc_no = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    BankBranch = table.Column<string>(nullable: true),
                    BankName = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Colorcode = table.Column<string>(nullable: true),
                    CountyID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Gst = table.Column<string>(nullable: true),
                    IFCCode = table.Column<string>(nullable: true),
                    MailingName = table.Column<string>(nullable: true),
                    Mob_no = table.Column<string>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    PhoneNo = table.Column<string>(nullable: true),
                    PinCode = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Tan = table.Column<string>(nullable: true),
                    Website = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CustomerTypes",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerTypes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Denominations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CODE = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    CurrencyID = table.Column<Guid>(nullable: false),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    Value = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Denominations", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Divisons",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Divisons", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Languages",
                columns: table => new
                {
                    LanguageID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Locale = table.Column<string>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Languages", x => x.LanguageID);
                });

            migrationBuilder.CreateTable(
                name: "products",
                columns: table => new
                {
                    ProductID = table.Column<Guid>(nullable: false),
                    BaseUnitID = table.Column<Guid>(nullable: false),
                    BrandID = table.Column<Guid>(nullable: false),
                    CODE = table.Column<string>(nullable: true),
                    CategoryID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    DivisionID = table.Column<Guid>(nullable: false),
                    EANBaseUnit = table.Column<string>(nullable: true),
                    GroupID = table.Column<Guid>(nullable: false),
                    HSNCode = table.Column<string>(nullable: true),
                    Inactive = table.Column<bool>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    OLD_CODE = table.Column<string>(nullable: true),
                    Perishable = table.Column<bool>(nullable: false),
                    PerishableDays = table.Column<string>(nullable: true),
                    ProductBatch = table.Column<bool>(nullable: false),
                    ProductSerial = table.Column<bool>(nullable: false),
                    ProductTypeID = table.Column<Guid>(nullable: false),
                    PurchaseBaseUnitID = table.Column<Guid>(nullable: false),
                    SubCategoryID = table.Column<Guid>(nullable: false),
                    TaxID = table.Column<Guid>(nullable: false),
                    purchasecost = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_products", x => x.ProductID);
                });

            migrationBuilder.CreateTable(
                name: "ProductTypes",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductTypes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    DisplayAccess = table.Column<bool>(nullable: false),
                    FullAccess = table.Column<bool>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    NoAccess = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Taxes",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    TaxFixed = table.Column<decimal>(nullable: false),
                    TaxPercentage = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Taxes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Tenders",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    ATM_API_Linked = table.Column<bool>(nullable: false),
                    Approval_Code = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    TenderType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tenders", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "TerminalTypes",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TerminalTypes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "UOMs",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UOMs", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    BioMatric = table.Column<string>(nullable: true),
                    City = table.Column<Guid>(nullable: false),
                    Country = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    District = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    InActive = table.Column<bool>(nullable: false),
                    MobileNo = table.Column<string>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    POSCode = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    PhoneNo = table.Column<string>(nullable: true),
                    Pincode = table.Column<string>(nullable: true),
                    RoleID = table.Column<Guid>(nullable: false),
                    State = table.Column<string>(nullable: true),
                    Website = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "VendorTypes",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VendorTypes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "subCategories",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CategoryID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_subCategories", x => x.ID);
                    table.ForeignKey(
                        name: "FK_subCategories_Categories_CategoryID",
                        column: x => x.CategoryID,
                        principalTable: "Categories",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompanyDetails",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CompanyID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    FirstDate = table.Column<DateTime>(nullable: false),
                    Isend = table.Column<bool>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    SecondDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyDetails", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CompanyDetails_Companies_CompanyID",
                        column: x => x.CompanyID,
                        principalTable: "Companies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BrandTranslation",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    BrandID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BrandTranslation", x => x.ID);
                    table.ForeignKey(
                        name: "FK_BrandTranslation_Brands_BrandID",
                        column: x => x.BrandID,
                        principalTable: "Brands",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BrandTranslation_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CategoryTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CategoryID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CategoryTranslations_Categories_CategoryID",
                        column: x => x.CategoryID,
                        principalTable: "Categories",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CategoryTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CityTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CityID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CityTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CityTranslations_Cities_CityID",
                        column: x => x.CityID,
                        principalTable: "Cities",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CityTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompanyTranslation",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    CompanyID = table.Column<Guid>(nullable: false),
                    ContactPerson = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    MailingName = table.Column<string>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyTranslation", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CompanyTranslation_Companies_CompanyID",
                        column: x => x.CompanyID,
                        principalTable: "Companies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompanyTranslation_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CountryTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    CountryID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CountryTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CountryTranslations_Countries_CountryID",
                        column: x => x.CountryID,
                        principalTable: "Countries",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CountryTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "currencyTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    CurrencyID = table.Column<Guid>(nullable: false),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_currencyTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_currencyTranslations_Currencies_CurrencyID",
                        column: x => x.CurrencyID,
                        principalTable: "Currencies",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_currencyTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CustomerTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    CustomerID = table.Column<Guid>(nullable: false),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CustomerTranslations_Customers_CustomerID",
                        column: x => x.CustomerID,
                        principalTable: "Customers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CustomerTypeTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    CustomerTypeName = table.Column<string>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    customertypeID = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerTypeTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CustomerTypeTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerTypeTranslations_CustomerTypes_customertypeID",
                        column: x => x.customertypeID,
                        principalTable: "CustomerTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DenominationTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    denominationID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DenominationTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_DenominationTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DenominationTranslations_Denominations_denominationID",
                        column: x => x.denominationID,
                        principalTable: "Denominations",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DivisonTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    divisionID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DivisonTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_DivisonTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DivisonTranslations_Divisons_divisionID",
                        column: x => x.divisionID,
                        principalTable: "Divisons",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GroupTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    groupID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_GroupTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GroupTranslations_Groups_groupID",
                        column: x => x.groupID,
                        principalTable: "Groups",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductTypeTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    producttypeID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductTypeTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ProductTypeTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductTypeTranslations_ProductTypes_producttypeID",
                        column: x => x.producttypeID,
                        principalTable: "ProductTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RoleTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    roleID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_RoleTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RoleTranslations_Roles_roleID",
                        column: x => x.roleID,
                        principalTable: "Roles",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "taxeTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    taxeID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_taxeTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_taxeTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_taxeTranslations_Taxes_taxeID",
                        column: x => x.taxeID,
                        principalTable: "Taxes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TenderTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    tenderID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenderTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TenderTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TenderTranslations_Tenders_tenderID",
                        column: x => x.tenderID,
                        principalTable: "Tenders",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TenderTypeTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    TerminalTypeName = table.Column<string>(nullable: true),
                    terminaltypeID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenderTypeTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TenderTypeTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TenderTypeTranslations_TerminalTypes_terminaltypeID",
                        column: x => x.terminaltypeID,
                        principalTable: "TerminalTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UOMTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    uomID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UOMTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UOMTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UOMTranslations_UOMs_uomID",
                        column: x => x.uomID,
                        principalTable: "UOMs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    UserID = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserTranslations_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VendorTypeTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    VendorTypeName = table.Column<string>(nullable: true),
                    VendortypeID = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VendorTypeTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_VendorTypeTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VendorTypeTranslations_VendorTypes_VendortypeID",
                        column: x => x.VendortypeID,
                        principalTable: "VendorTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SubCategorytranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    subcategoryID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubCategorytranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SubCategorytranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubCategorytranslations_subCategories_subcategoryID",
                        column: x => x.subcategoryID,
                        principalTable: "subCategories",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompanyDetailTranslations",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    LanguageID = table.Column<Guid>(nullable: false),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    companiedetailsID = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyDetailTranslations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CompanyDetailTranslations_Languages_LanguageID",
                        column: x => x.LanguageID,
                        principalTable: "Languages",
                        principalColumn: "LanguageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompanyDetailTranslations_CompanyDetails_companiedetailsID",
                        column: x => x.companiedetailsID,
                        principalTable: "CompanyDetails",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BrandTranslation_BrandID",
                table: "BrandTranslation",
                column: "BrandID");

            migrationBuilder.CreateIndex(
                name: "IX_BrandTranslation_LanguageID",
                table: "BrandTranslation",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryTranslations_CategoryID",
                table: "CategoryTranslations",
                column: "CategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryTranslations_LanguageID",
                table: "CategoryTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_CityTranslations_CityID",
                table: "CityTranslations",
                column: "CityID");

            migrationBuilder.CreateIndex(
                name: "IX_CityTranslations_LanguageID",
                table: "CityTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyDetails_CompanyID",
                table: "CompanyDetails",
                column: "CompanyID");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyDetailTranslations_LanguageID",
                table: "CompanyDetailTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyDetailTranslations_companiedetailsID",
                table: "CompanyDetailTranslations",
                column: "companiedetailsID");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyTranslation_CompanyID",
                table: "CompanyTranslation",
                column: "CompanyID");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyTranslation_LanguageID",
                table: "CompanyTranslation",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_CountryTranslations_CountryID",
                table: "CountryTranslations",
                column: "CountryID");

            migrationBuilder.CreateIndex(
                name: "IX_CountryTranslations_LanguageID",
                table: "CountryTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_currencyTranslations_CurrencyID",
                table: "currencyTranslations",
                column: "CurrencyID");

            migrationBuilder.CreateIndex(
                name: "IX_currencyTranslations_LanguageID",
                table: "currencyTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerTranslations_CustomerID",
                table: "CustomerTranslations",
                column: "CustomerID");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerTranslations_LanguageID",
                table: "CustomerTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerTypeTranslations_LanguageID",
                table: "CustomerTypeTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerTypeTranslations_customertypeID",
                table: "CustomerTypeTranslations",
                column: "customertypeID");

            migrationBuilder.CreateIndex(
                name: "IX_DenominationTranslations_LanguageID",
                table: "DenominationTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_DenominationTranslations_denominationID",
                table: "DenominationTranslations",
                column: "denominationID");

            migrationBuilder.CreateIndex(
                name: "IX_DivisonTranslations_LanguageID",
                table: "DivisonTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_DivisonTranslations_divisionID",
                table: "DivisonTranslations",
                column: "divisionID");

            migrationBuilder.CreateIndex(
                name: "IX_GroupTranslations_LanguageID",
                table: "GroupTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_GroupTranslations_groupID",
                table: "GroupTranslations",
                column: "groupID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTypeTranslations_LanguageID",
                table: "ProductTypeTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTypeTranslations_producttypeID",
                table: "ProductTypeTranslations",
                column: "producttypeID");

            migrationBuilder.CreateIndex(
                name: "IX_RoleTranslations_LanguageID",
                table: "RoleTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_RoleTranslations_roleID",
                table: "RoleTranslations",
                column: "roleID");

            migrationBuilder.CreateIndex(
                name: "IX_subCategories_CategoryID",
                table: "subCategories",
                column: "CategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_SubCategorytranslations_LanguageID",
                table: "SubCategorytranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_SubCategorytranslations_subcategoryID",
                table: "SubCategorytranslations",
                column: "subcategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_taxeTranslations_LanguageID",
                table: "taxeTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_taxeTranslations_taxeID",
                table: "taxeTranslations",
                column: "taxeID");

            migrationBuilder.CreateIndex(
                name: "IX_TenderTranslations_LanguageID",
                table: "TenderTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_TenderTranslations_tenderID",
                table: "TenderTranslations",
                column: "tenderID");

            migrationBuilder.CreateIndex(
                name: "IX_TenderTypeTranslations_LanguageID",
                table: "TenderTypeTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_TenderTypeTranslations_terminaltypeID",
                table: "TenderTypeTranslations",
                column: "terminaltypeID");

            migrationBuilder.CreateIndex(
                name: "IX_UOMTranslations_LanguageID",
                table: "UOMTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_UOMTranslations_uomID",
                table: "UOMTranslations",
                column: "uomID");

            migrationBuilder.CreateIndex(
                name: "IX_UserTranslations_LanguageID",
                table: "UserTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_UserTranslations_UserID",
                table: "UserTranslations",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_VendorTypeTranslations_LanguageID",
                table: "VendorTypeTranslations",
                column: "LanguageID");

            migrationBuilder.CreateIndex(
                name: "IX_VendorTypeTranslations_VendortypeID",
                table: "VendorTypeTranslations",
                column: "VendortypeID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BrandTranslation");

            migrationBuilder.DropTable(
                name: "CategoryTranslations");

            migrationBuilder.DropTable(
                name: "CityTranslations");

            migrationBuilder.DropTable(
                name: "CompanyDetailTranslations");

            migrationBuilder.DropTable(
                name: "CompanyTranslation");

            migrationBuilder.DropTable(
                name: "CountryTranslations");

            migrationBuilder.DropTable(
                name: "currencyTranslations");

            migrationBuilder.DropTable(
                name: "CustomerTranslations");

            migrationBuilder.DropTable(
                name: "CustomerTypeTranslations");

            migrationBuilder.DropTable(
                name: "DenominationTranslations");

            migrationBuilder.DropTable(
                name: "DivisonTranslations");

            migrationBuilder.DropTable(
                name: "GroupTranslations");

            migrationBuilder.DropTable(
                name: "products");

            migrationBuilder.DropTable(
                name: "ProductTypeTranslations");

            migrationBuilder.DropTable(
                name: "RoleTranslations");

            migrationBuilder.DropTable(
                name: "SubCategorytranslations");

            migrationBuilder.DropTable(
                name: "taxeTranslations");

            migrationBuilder.DropTable(
                name: "TenderTranslations");

            migrationBuilder.DropTable(
                name: "TenderTypeTranslations");

            migrationBuilder.DropTable(
                name: "UOMTranslations");

            migrationBuilder.DropTable(
                name: "UserTranslations");

            migrationBuilder.DropTable(
                name: "VendorTypeTranslations");

            migrationBuilder.DropTable(
                name: "Cities");

            migrationBuilder.DropTable(
                name: "CompanyDetails");

            migrationBuilder.DropTable(
                name: "Countries");

            migrationBuilder.DropTable(
                name: "Currencies");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "CustomerTypes");

            migrationBuilder.DropTable(
                name: "Denominations");

            migrationBuilder.DropTable(
                name: "Divisons");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "ProductTypes");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "subCategories");

            migrationBuilder.DropTable(
                name: "Taxes");

            migrationBuilder.DropTable(
                name: "Tenders");

            migrationBuilder.DropTable(
                name: "TerminalTypes");

            migrationBuilder.DropTable(
                name: "UOMs");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Languages");

            migrationBuilder.DropTable(
                name: "VendorTypes");

            migrationBuilder.DropTable(
                name: "Companies");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
