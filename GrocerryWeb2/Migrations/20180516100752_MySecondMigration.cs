﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GrocerryWeb2.Migrations
{
    public partial class MySecondMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Brands",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Colorcode = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: true),
                    Created_User = table.Column<Guid>(nullable: true),
                    Deleted = table.Column<bool>(nullable: true),
                    Deleted_By = table.Column<Guid>(nullable: true),
                    Deleted_Date = table.Column<DateTime>(nullable: true),
                    Modified_Date = table.Column<DateTime>(nullable: true),
                    Modified_User = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Brands", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Brands");
        }
    }
}
