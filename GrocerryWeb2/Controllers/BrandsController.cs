﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GrocerryWeb2.Data;
using GrocerryWeb2.Model;
using Newtonsoft.Json.Linq;

namespace GrocerryWeb2.Controllers
{
    [Produces("application/json")]
    [Route("api/Brands")]
    public class BrandsController : Controller
    {
        private readonly GrocerryWebDB _context;

        public BrandsController(GrocerryWebDB context)
        {
            _context = context;
           
           
        }

        // GET: api/Brands
        [HttpGet]
        public IEnumerable<Brand> GetBrands()
        {
            return _context.Brands;
        }

        // GET: api/Brands/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBrand([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var brand = await _context.Brands.SingleOrDefaultAsync(m => m.ID == id);

            if (brand == null)
            {
                return NotFound();
            }

            return Ok(brand);
        }

        // PUT: api/Brands/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBrand([FromRoute] Guid id, [FromBody] Brand brand)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != brand.ID)
            {
                return BadRequest();
            }

            _context.Entry(brand).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BrandExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Brands
        [HttpPost]
        public async Task<IActionResult> PostBrand([FromBody]JObject  brand)
        {
          var obj = brand.ToString();
          var result = brand.ToObject<Dictionary<string, object>>();
          //var a = result["code"];
          //var  c = result["colorcode"];

            Brand b = new Brand();
            
            b.ID = Guid.NewGuid();
            b.Code= Convert.ToString(result["code"]);
            b.Colorcode = Convert.ToString(result["colorcode"]);
            b.Deleted = true;

            _context.Brands.Add(b);
            _context.SaveChanges();            
            await _context.SaveChangesAsync();
            return RedirectToAction("BrandManage");
         }

        // DELETE: api/Brands/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBrand([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var brand = await _context.Brands.SingleOrDefaultAsync(m => m.ID == id);
            if (brand == null)
            {
                return NotFound();
            }

            _context.Brands.Remove(brand);
            await _context.SaveChangesAsync();

            return Ok(brand);
        }

        private bool BrandExists(Guid id)
        {
            return _context.Brands.Any(e => e.ID == id);
        }
    }
}