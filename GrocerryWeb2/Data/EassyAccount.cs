﻿using GrocerryWeb2.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace GrocerryWeb2.Data
{
    public class GrocerryWebDB:DbContext
    {
        public GrocerryWebDB(DbContextOptions<GrocerryWebDB> options):base(options)
        {

        }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<BrandTranslation> BrandTranslation { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CategoryTranslation> CategoryTranslations { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<CityTranslation>CityTranslations{ get; set; }
        public DbSet<Company>Companies{ get; set; }
        public DbSet<CompanyDetail> CompanyDetails { get; set; }
        public DbSet<CompanyTranslation> CompanyTranslation { get; set; }
        public DbSet<CompanyDetailTranslation> CompanyDetailTranslations { get; set; }
        public DbSet<Country>Countries{ get; set; }
        public DbSet<CountryTranslation>CountryTranslations { get; set; }
        public DbSet<Currency>Currencies { get; set; }
        public DbSet<CurrencyTranslation>currencyTranslations { get; set; }
        public DbSet<Customer>Customers{ get; set; }
        public DbSet<CustomerTranslation> CustomerTranslations { get; set; }
        public DbSet<CustomerType> CustomerTypes { get; set; }
        public DbSet<CustomerTypeTranslation> CustomerTypeTranslations { get; set; }
        public DbSet<Denomination> Denominations { get; set; }
        public DbSet<DenominationTranslation> DenominationTranslations { get; set; }
        public DbSet<Divison>Divisons { get; set; }
        public DbSet<DivisonTranslation> DivisonTranslations { get; set; }
        public DbSet<Group> Groups { get; set; }

        public DbSet<GroupTranslation> GroupTranslations { get; set; }
        public DbSet<Languages> Languages { get; set; }
      
        public DbSet<Product>products { get; set; }
        public DbSet<ProductType>ProductTypes { get; set; }
        public DbSet<ProductTypeTranslation>ProductTypeTranslations { get; set; }
        public DbSet<Role>Roles { get; set; }
        public DbSet<RoleTranslation>RoleTranslations { get; set; }

        public DbSet<SubCategory>subCategories { get; set; }

        public DbSet<SubCategorytranslation>SubCategorytranslations{ get; set; }

        public DbSet<Taxe>Taxes { get; set; }

        public DbSet<TaxeTranslation>taxeTranslations { get; set; }

        public DbSet<Tender>Tenders { get; set; }

        public DbSet<TenderTranslation> TenderTranslations { get; set; }


          public DbSet<TerminalType>TerminalTypes { get; set; }


          public DbSet<TerminalTypeTranslation> TenderTypeTranslations { get; set; }


          public DbSet<UOM>UOMs{ get; set; }

         public DbSet<UOMTranslation>UOMTranslations { get; set; }

          public DbSet<User>Users { get; set; }

          public DbSet<UserTranslation>UserTranslations { get; set; }

          public DbSet<VendorType>VendorTypes { get; set; }
         public DbSet<VendorTypeTranslation>VendorTypeTranslations { get; set; }
        



        
        







    }

}

  

